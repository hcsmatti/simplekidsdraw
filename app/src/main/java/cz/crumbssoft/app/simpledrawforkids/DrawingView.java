package cz.crumbssoft.app.simpledrawforkids;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import static java.lang.Integer.compare;


public class DrawingView extends View {

    private boolean erase = false;
    private float brushSize;
    //drawing path
    private Path drawPath;
    //drawing and canvas paint
    private final Paint drawPaint = DataHolder.getInstance().getDrawPaint();
    private final Paint canvasPaint = DataHolder.getInstance().getCanvasPaint();
    //initial color
    private int paintColor = 0xFF660000;
    private int oldColor = 1;
    //canvas
    private Canvas drawCanvas;

    private static final int SELECT_PICTURE = 1;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupDrawing();
    }

    public void setBrushSize(float newSize) {
        brushSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                newSize, getResources().getDisplayMetrics());

        drawPaint.setStrokeWidth(brushSize);
    }

    public void setErase(boolean isErase) {
        erase = isErase;
        if (isErase) {
            this.oldColor = this.paintColor;
            this.setColor("#FFFFFFFF");
        } else {
            paintColor = oldColor;
            drawPaint.setColor(paintColor);
        }
    }

    public void startNew() {
        drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    public void openImage(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        int pWidth = bitmap.getWidth();
        int pHeight = bitmap.getHeight();
        int sWidth = drawCanvas.getWidth();
        int sHeight = drawCanvas.getHeight();
        double widthRatio = (double) pWidth / (double) sWidth;
        double heightRatio =(double)  pHeight / (double) sHeight;
        double minRatio = widthRatio < heightRatio ? widthRatio : heightRatio;
        int targetWidth = (int) (pWidth / minRatio);
        int targetHeight = (int) (pHeight / minRatio);
        float startX = 0f;
        float startY = 0f;
        if (compare(targetWidth, sWidth) != 0){
            startX = (targetWidth - sWidth) / 2f;
        }
        if (compare(targetHeight, sHeight) != 0){
            startY = (targetHeight - sHeight) / 2f;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, targetWidth, targetHeight, true);
        invalidate();
        drawCanvas.drawBitmap(bitmap, -startX, -startY, null);
    }

    private void setupDrawing() {
        brushSize = getResources().getInteger(R.integer.medium_size);
        drawPath = new Path();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(brushSize);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (DataHolder.getInstance().getBitmap() == null) {
            DataHolder.getInstance().setBitmap(Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888));
        } else {
            Bitmap resizedBitmap = BitmapUtils.resizeBitmap(DataHolder.getInstance().getBitmap(), w, h);
            DataHolder.getInstance().setBitmap(Bitmap.createBitmap(resizedBitmap));
        }
        drawCanvas = new Canvas(DataHolder.getInstance().getBitmap());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(DataHolder.getInstance().getBitmap(), 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawCanvas.drawPoint(touchX, touchY, drawPaint);
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public void setColor(String newColor) {
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
    }

    public boolean isErase() {
        return erase;
    }


}
