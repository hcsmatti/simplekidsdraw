package cz.crumbssoft.app.simpledrawforkids;

import android.graphics.Bitmap;
import android.graphics.Paint;

public class DataHolder {
    private static DataHolder instance;
    private Paint drawPaint = new Paint();
    private Paint canvasPaint = new Paint(Paint.DITHER_FLAG);
    private Bitmap bitmap;

    private DataHolder() {
    }

    public static synchronized DataHolder getInstance() {
        if (instance == null) {
            instance = new DataHolder();
        }
        return instance;
    }

    public Paint getDrawPaint() {
        return drawPaint;
    }

    public void setDrawPaint(Paint drawPaint) {
        this.drawPaint = drawPaint;
    }

    public Paint getCanvasPaint() {
        return canvasPaint;
    }

    public void setCanvasPaint(Paint canvasPaint) {
        this.canvasPaint = canvasPaint;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
