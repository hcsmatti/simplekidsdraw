package cz.crumbssoft.app.simpledrawforkids;

import android.graphics.Bitmap;

public class BitmapUtils {

    public static Bitmap resizeBitmap(Bitmap originalBitmap, int newWidth, int newHeight) {
        return Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, true);
    }
}