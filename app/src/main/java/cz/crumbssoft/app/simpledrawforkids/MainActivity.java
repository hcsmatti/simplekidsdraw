package cz.crumbssoft.app.simpledrawforkids;

import static cz.crumbssoft.app.simpledrawforkids.R.id.stroke_btn;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

public class MainActivity extends Activity implements OnClickListener {
    private static final int REQ_CODE_PICK_IMAGE = 1;
    private ImageButton currPaint;
    private ImageButton drawBtn;
    private ImageButton eraseBtn;
    private ImageButton strokeBtn;
    private ImageButton infoBtn;
    private float smallBrush, mediumBrush, largeBrush;
    private DrawingView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);
        drawView = findViewById(R.id.drawing);
        drawView.setSaveEnabled(true);
        LinearLayout paintLayout = findViewById(R.id.paint_colors);
        currPaint = (ImageButton) paintLayout.getChildAt(0);
        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.border_paint_selected));
        drawView.setBrushSize(mediumBrush);
        ImageButton newBtn = findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);
        ImageButton openBtn = findViewById(R.id.open_btn);
        openBtn.setOnClickListener(this);
        ImageButton saveBtn = findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);
        strokeBtn = findViewById(stroke_btn);
        strokeBtn.setOnClickListener(this);
        drawBtn = findViewById(R.id.draw_btn);
        eraseBtn = findViewById(R.id.erase_btn);
        infoBtn = findViewById(R.id.info_btn);
        infoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.info_btn) {
            showInfoDialog();
        } else if (id == stroke_btn) {
            final Dialog strokeDialog = getStrokeDialog();
            strokeDialog.show();
        } else if (id == R.id.new_btn) {
            AlertDialog.Builder newDialog = getNewDialog();
            newDialog.show();
        } else if (id == R.id.save_btn) {
            AlertDialog.Builder saveDialog = getSaveDialog();
            saveDialog.show();
        } else if (id == R.id.open_btn) {
            AlertDialog.Builder openDialog = getOpenDialog();
            openDialog.show();
        }
    }

    private void showInfoDialog() {
        String homeUrl = "https://www.crumbs-software.cz/#/home";
        String drawUrl = "https://www.crumbs-software.cz/#/draw";
        String privacyPolicyUrl = "https://www.crumbs-software.cz/#/draw/privacy-policy";

        String formattedMessage = String.format(getString(R.string.info_text), drawUrl, homeUrl, privacyPolicyUrl);

        SpannableString spannableText = new SpannableString(formattedMessage);
        URLSpan homeUrlSpan = new URLSpan(homeUrl);
        URLSpan drawUrlSpan = new URLSpan(drawUrl);
        URLSpan privacyPolicyUrlSpan = new URLSpan(privacyPolicyUrl);

        int startIndexPPolicy = formattedMessage.indexOf(privacyPolicyUrl);
        int endIndexPPolicy = startIndexPPolicy + privacyPolicyUrl.length();
        spannableText.setSpan(privacyPolicyUrlSpan, startIndexPPolicy, endIndexPPolicy, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int startIndexDraw = formattedMessage.indexOf(drawUrl);
        int endIndexDraw = startIndexDraw + drawUrl.length();
        spannableText.setSpan(drawUrlSpan, startIndexDraw, endIndexDraw, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        int startIndexHome = formattedMessage.indexOf(homeUrl);
        int endIndexHome = startIndexHome + homeUrl.length();
        spannableText.setSpan(homeUrlSpan, startIndexHome, endIndexHome, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        AlertDialog infoDialog = new AlertDialog.Builder(this)
                .setCancelable(true)
                .setPositiveButton("OK", null)
                .setTitle(R.string.info_title)
                .setMessage(spannableText)
                .show();

        TextView messageView = infoDialog.findViewById(android.R.id.message);
        if (messageView != null) {
            messageView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    private AlertDialog.Builder getNewDialog() {
        AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
        newDialog.setTitle(R.string.new_title);
        newDialog.setMessage(R.string.new_warning);
        newDialog.setPositiveButton(R.string.yes, (dialog, which) -> {
            drawView.startNew();
            dialog.dismiss();
        });
        newDialog.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        return newDialog;
    }

    private AlertDialog.Builder getSaveDialog() {
        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
        saveDialog.setTitle(R.string.save_title);
        saveDialog.setMessage(R.string.save_warning);
        saveDialog.setPositiveButton(R.string.yes, (dialog, which) -> {
            drawView.setDrawingCacheEnabled(true);
            String imgSaved = MediaStore.Images.Media.insertImage(
                    getContentResolver(), drawView.getDrawingCache(),
                    UUID.randomUUID().toString() + ".png", "drawing");
            if (imgSaved != null) {
                Toast savedToast = Toast.makeText(getApplicationContext(),
                        R.string.save_success, Toast.LENGTH_SHORT);
                savedToast.show();
            } else {
                Toast unsavedToast = Toast.makeText(getApplicationContext(),
                        R.string.save_failed, Toast.LENGTH_SHORT);
                unsavedToast.show();
            }
            drawView.destroyDrawingCache();
        });
        saveDialog.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        return saveDialog;
    }

    private AlertDialog.Builder getOpenDialog() {
        AlertDialog.Builder openDialog = new AlertDialog.Builder(this);
        openDialog.setTitle(R.string.open_title);
        openDialog.setMessage(R.string.open_warning);
        openDialog.setPositiveButton(R.string.yes, (dialog, which) -> {
            Intent choosePictureIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(choosePictureIntent, REQ_CODE_PICK_IMAGE);
        });
        openDialog.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        return openDialog;
    }

    private Dialog getStrokeDialog() {
        final Dialog strokeDialog = new Dialog(this);
        strokeDialog.setTitle(R.string.stroke_size);
        strokeDialog.setContentView(R.layout.stroke_chooser);
        ImageButton smallBtn = strokeDialog.findViewById(R.id.small_brush);
        smallBtn.setOnClickListener(v -> {
            drawView.setBrushSize(smallBrush);
            strokeBtn.setBackgroundResource(R.drawable.small);
            strokeDialog.dismiss();
        });
        ImageButton mediumBtn = strokeDialog.findViewById(R.id.medium_brush);
        mediumBtn.setOnClickListener(v -> {
            drawView.setBrushSize(mediumBrush);
            strokeBtn.setBackgroundResource(R.drawable.medium);
            strokeDialog.dismiss();
        });
        ImageButton largeBtn = strokeDialog.findViewById(R.id.large_brush);
        largeBtn.setOnClickListener(v -> {
            drawView.setBrushSize(largeBrush);
            strokeBtn.setBackgroundResource(R.drawable.large);
            strokeDialog.dismiss();
        });
        return strokeDialog;
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case REQ_CODE_PICK_IMAGE:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(
                            selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    drawView.openImage(filePath);
                }
        }
    }

    public void strokeClicked(View view) {
        final Dialog strokeDialog = getStrokeDialog();
        strokeDialog.show();
    }

    public void brushClicked(View view) {
        if (drawView.isErase()){
        drawView.setErase(false);
        }
        changeActiveButton(drawView.isErase());
    }

    public void rubberClicked(View view) {
        drawView.setErase(true);
        changeActiveButton(drawView.isErase());
    }

    public void paintClicked(View view) {
        if (view != currPaint) {
            drawView.setErase(false);
            changeActiveButton(drawView.isErase());
            ImageButton imgView = (ImageButton) view;
            String color = view.getTag().toString();
            drawView.setColor(color);
            imgView.setImageDrawable(getResources().getDrawable(R.drawable.border_paint_selected));
            currPaint.setImageDrawable(getResources().getDrawable(R.drawable.border_paint_default));
            currPaint = (ImageButton) view;
        }
    }

    public void changeActiveButton(boolean isErase) {
        if (isErase) {
            drawBtn.setBackgroundResource(R.drawable.brush_default);
            eraseBtn.setBackgroundResource(R.drawable.rubber_selected);
        } else {
            drawBtn.setBackgroundResource(R.drawable.brush_selected);
            eraseBtn.setBackgroundResource(R.drawable.rubber_default);
        }
    }

}
